# -*- coding: utf-8 -*-
{
    # Basic
    'name': "Open Academy",
    'depends': ['base',],
    'data': [
        "security/security.xml",
        "security/ir.model.access.csv",
        "views/academy_course_views.xml",
        "views/academy_session_views.xml",
        "views/res_partner_views.xml",
        "views/academy_menus.xml",
    ],
    "auto_install": False,
    "application": True,
    "installable": True,

    # Advanced
    "version": "1.0",
    'summary': """Building A Module Odoo/15.0 (Community)""",
    'description': """Building A Module Odoo/15.0 (Community)""",
    "author": "LKD",
    "website": "",
    "license": "LGPL-3",
    "category": "Open Academy/Roles",
    "demo": [
        "demo/academy_demo.xml",
    ],
    "external_dependencies": {},
    "assets": {},
    "maintainer": "",
    "pre_init_hook": "",
    "post_init_hook": "",
    "uninstall_hook": "",
}
