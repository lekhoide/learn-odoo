# -*- coding: utf-8 -*-

from odoo import models, fields, api


class Course(models.Model):

    # -- Private Attributes --

    _name = 'academy.course'
    _description = 'Academy Course'
    _sql_constraints = [
        ("check_course_name", "UNIQUE(name)", "The course name unique"),
        ("check_course_description_and_title", "CHECK(name NOT LIKE description)", "The course descrption and the course title must different"),
    ]

    # -- Fields Declaration --

    # Basic
    name = fields.Char("Title", required=True)
    description = fields.Text("Description")

    # Relational
    responsible_id = fields.Many2one("res.users", string="Responsible")
    session_ids = fields.One2many("academy.session", "course_id")

    # -- Constrains and Onchanges --

    @api.returns('self', lambda value: value.id)
    def copy(self, default=None):
        default = dict(default or {}, name=("Copy of %s") % (self.name))
        return super(Course, self).copy(default=default)


