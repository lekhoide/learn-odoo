# -*- coding: utf-8 -*-

from odoo import models, fields, api
from odoo.exceptions import UserError, ValidationError

class Session(models.Model):

    # -- Private Attributes --

    _name = "academy.session"
    _description = "Academy Session"

    # -- Default Methods

    def _default_start_date(self):
        return fields.Date.today()

    # -- Fields Declaration --

    # Basic
    name = fields.Char("Name")
    start_date = fields.Date("Start Date", default=lambda self: self._default_start_date())
    duration = fields.Float("Duration")
    seat = fields.Integer("Seats", help="Number of seats")
    active = fields.Boolean("Active", default=True)

    # Computed
    taken_seats = fields.Float("Taken Seats", compute="_compute_taken_seats")
    attendees_count = fields.Integer("Attendees Count", compute="_compute_attendees", store=True)

    # Relational
    instructor_id = fields.Many2one(
        "res.partner",
        string="Instructor",
        domain="['|',('instructor', '=', True), '|', ('category_id', '=', 'Teacher / Level 1'), ('category_id', '=', 'Teacher / Level 2')]")
    course_id = fields.Many2one("academy.course", string="Course", required=True)
    attendees_ids = fields.Many2many("res.partner", string="Attendees")

    # -- Compute Methods --

    @api.depends("seat", "attendees_ids")
    def _compute_taken_seats(self):
        for session in self:
            attendees = len(session.attendees_ids.mapped("name"))
            session.taken_seats = (attendees * 100.0) / session.seat

    @api.depends("attendees_ids")
    def _compute_attendees(self):
        for session in self:
            attendees = len(session.attendees_ids.mapped("name"))
            if session.attendees_ids:
                session.attendees_count = attendees
            else:
                session.attendees_count = 0

    # -- Constrains and Onchanges --

    @api.constrains("instructor_id", "attendees_ids")
    def _check_instructor(self):
        for record in self:
            if record.instructor_id in record.attendees_ids:
                raise ValidationError("The instructor '%s' is not present in the attendees." % record.instructor_id.name)


    @api.onchange("seat")
    def _onchange_seat(self):
        """ Invalid values like a negative number of seats
        More participants than seats
        """
        if self.seat < 0:
            raise ValidationError("The seat attribute cannot be a negative number.")
        attendees = len(self.attendees_ids.mapped("name"))
        if attendees > self.seat:
            raise ValidationError("More participants than seats.")
