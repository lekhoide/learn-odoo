# -*- coding: utf-8 -*-

from odoo import models, fields, api

class ResPartner(models.Model):

    # -- Private Attribute --

    _inherit = "res.partner"

    # -- Fields Declaration --

    # Sepical
    instructor = fields.Boolean("Is Instructor")

    # Relational
    session_ids = fields.Many2many("academy.session", string="Session")