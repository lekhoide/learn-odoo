# -*- coding: utf-8 -*-
{
    # Basic
    'name': "Base From DeLe",
    'version': '1.0', # follow semantic versioning rules
    'description': "",
    'author': "dele",
    'website': "",
    'license': "LGPL-3", # odoo/addons/base/models/ir_module.py
    'category': "Uncategorized",
    'depends': [],
    'data': [],
    'demo': [],

    # Advanced
    'auto_install': False,  # 'auto_install': [],
    'external_dependencies': {
        'python': [],
        'bin': []
    },
    'application': False,  # a fully-fledged application : True - a technical module : False
    'assets': {},
    'installable': True,
    'maintainer': "dele",
    'pre_init_hook': "",
    'post_init_hook': "",
    'uninstall_hook': "",
}
