# -*- coding: utf-8 -*-
{
    # Basic
    'name': "Cafe Login Page",
    'version': '1.0', # follow semantic versioning rules
    'description': """
        01/09/2022: Building manifest
    """,
    'author': "dele",
    'website': "",
    'license': "LGPL-3", #odoo/addons/base/models/ir_module.py
    'category': "Uncategorized",
    'depends': ['web'],
    'data': [
        # Front End Views
        # ============================================================
        'views/cafe_login_templates.xml',
        # ============================================================
    ],
    'demo': [],

    # Advanced
    'auto_install': False,  # 'auto_install': [],
    'external_dependencies': {
        'python': [],
        'bin': []
    },
    'application': True,  # a fully-fledged application : True - a technical module : False
    'assets': {},
    'installable': True,
    'maintainer': "dele",
    "pre_init_hook": "",
    "post_init_hook": "",
    "uninstall_hook": "",
}
